from Day2 import sum_list


def test_sum_123():
    assert sum_list(['1', '2', '3']) == 6


def test_sum_531():
    assert sum_list(['1', '3', '5']) == 9
