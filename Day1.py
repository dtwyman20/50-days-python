import math


def divide_or_square(num):
    if num % 5 == 0:
        return round(math.sqrt(num), 2)
    else:
        return num % 5


print(divide_or_square(5))    # prints 2.24
print(divide_or_square(25))   # prints 5.00
print(divide_or_square(10))   # prints 3.16
print(divide_or_square(6))    # prints 1
print(divide_or_square(225))  # prints 15.00
